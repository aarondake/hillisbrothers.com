<!DOCTYPE html>
<html lang="en">
<head>
<title>Estimate</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1>Request An Estimate</h1>
				<h3 class="c3">On-Site Appointment</h3>
				<p>Use this calendar to schedule a free on-site appointment with one of our expert painters!  You will receive a quote right there on the spot!</p>
				<p>If you prefer to talk to us first, just call <strong>616-554-5140.</p>
				<p>Thanks!</p>
				<p>-The Hillis Brothers</p>
				<?php include 'bookfresh.html'; ?>
			</div>
		</div>	
	</div>
</div>

<?php include('footer.php'); ?>