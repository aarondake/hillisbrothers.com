<div class="menu_wrapper">
<div class="container">
<div class="row">
<div class="span12">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
				<li id="btnHome"><a href="index.php">Home</a></li>
				<li id="btnAbout"><a href="about.php">About</a></li>
				<li id="btnTestimonials"><a href="testimonials.php">Testimonials</a></li>
				<!--<li id="btnLocations" class="sub-menu sub-menu-1"><a href="#">Locations</a>
					<ul>
						<li><a href="">Ada</a></li>
						<li><a href="">Belmont</a></li>
						<li><a href="">Byron Center</a></li>
						<li><a href="">Belding</a></li>
						<li><a href="">Cascade</a></li>
						<li><a href="">Caledonia</a></li>
						<li><a href="">Cedar Springs</a></li>
						<li><a href="">Forest Hillis</a></li>
						<li><a href="">Grand Rapids</a></li>
						<li><a href="">Kentwood</a></li>
						<li><a href="">Lowell</a></li>
						<li><a href="">Northview</a></li>
						<li><a href="">Rockford</a></li>
						<li><a href="">Sparta</a></li>
						<li><a href="">Walker</a></li>
						<li><a href="">Wyoming</a></li>
					</ul>
						<li class="sub-menu sub-menu-2">Kent County
							<ul>
								
							</ul>
						</li>		
						<li class="sub-menu sub-menu-2"><a href="decks.php">Muskegon County</a>
							<ul>
								<li><a href="">Muskegon</a></li>
								<li><a href="">Mona Shores</a></li>
								<li><a href="">Norton Shores</a></li>
								<li><a href="">North Muskegon</a></li>
								<li><a href="">Spring Lake</a></li>
							</ul>
						</li>
						
						</li>
						<li class="sub-menu sub-menu-2">Ottawa County
							<ul>
								<li><a href="">Grandville</a></li>
								<li><a href="">Jenison</a></li>
								<li><a href="">Grand Haven</a></li>
								<li><a href="">Nunica</a></li>
								<li><a href="">Dennison</a></li>
								<li><a href="">Marne</a></li>
								<li><a href="">Georgetown</a></li>
								<li><a href="">Coopersville</a></li>
								<li><a href="">West Olive</a></li>
								<li><a href="">Port Sheldon</a></li>
								<li><a href="">Waverly</a></li>
								<li><a href="">Zeeland</a></li>
								<li><a href="">Jamestown</a></li>
								<li><a href="">Spring Lake</a></li>
								<li><a href="">Hudsonville</a></li>
								<li><a href="">Harrisburg</a></li>
								<li><a href="">Conklin</a></li>
								<li><a href="">Reno</a></li>
								<li><a href="">Wright</a></li>
								<li><a href="">Chester</a></li>
								<li><a href="">Holland</a></li>
							</ul>
						</li>
						<li class="sub-menu sub-menu-2">Allegan County
							<ul>
								<li><a href="">Holland City</a></li>
								<li><a href="">Laktown</a></li>
								<li><a href="">Fillmore</a></li>
								<li><a href="">Overisel</a></li>
								<li><a href="">Salem</a></li>
								<li><a href="">Dorr</a></li>
								<li><a href="">Leighton</a></li>
								<li><a href="">Saugatuck</a></li>
								<li><a href="">Manlius</a></li>
								<li><a href="">Heath</a></li>
								<li><a href="">Monterey</a></li>
								<li><a href="">Hopkins</a></li>
								<li><a href="">Wayland</a></li>
								<li><a href="">Ganges</a></li>
								<li><a href="">Clyde</a></li>
								<li><a href="">Valley</a></li>
								<li><a href="">Allegan</a></li>
								<li><a href="">Watson</a></li>
								<li><a href="">Martin</a></li>
								<li><a href="">Casco</a></li>
								<li><a href="">Lee</a></li>
								<li><a href="">Cheshire</a></li>
								<li><a href="">Trowbridge</a></li>
								<li><a href="">Otsego</a></li>
								<li><a href="">Gun Plain</a></li>
							</ul>
						</li>
						<li class="sub-menu sub-menu-2">Barry County
							<ul>
								<li><a href="">Hastings</a></li>
							</ul>
						</li>
						<li class="sub-menu sub-menu-2"><a href="decks.php">Ionia County</a></li>
						<li class="sub-menu sub-menu-2"><a href="decks.php">Montcalm County</a></li>
						<li class="sub-menu sub-menu-2"><a href="decks.php">Newego County</a>
							<ul>
								<li><a href="">Greenville</a>
								<li><a href="">Cedar Springs</a></li>
							</ul>
						</li>
						<!--<li id="menuGutters"><a href="gutters.php">Gutter Cleaning</a></li>
					</ul>						
				</li>-->

				<li id="btnServices" class="sub-menu sub-menu-1"><a href="#">Services</a>
					<ul>
						<li class="sub-menu sub-menu-2">Painting
							<ul>
								<li id="menuInterior"><a href="interior.php">Interior</a></li>
								<li id="menuExterior"><a href="exterior.php">Exterior</a></li>
								<li id="menuCommercial"><a href="commercial.php">Commercial</a></li>
							</ul>
						</li>		
						<li id="menuDecks"><a href="decks.php">Decks</a></li>                        
                        <li id="menuDecks1"><a href="ada-painting.php">Ada Painting</a></li>
                          <li id="menuDecks2"><a href="byron-center-painting.php">Byron Center Painting</a></li>
                            <li id="menuDecks3"><a href="grand-haven-painting.php">Grand Haven Painting</a></li>
                              <li id="menuDecks4"><a href="grand-rapids-interior-painting.php">Grand Rapids Interior Painting</a></li>
                                <li id="menuDecks5"><a href="grandville-painting.php">Grandville Painting</a></li>
                                  <li id="menuDecks6"><a href="jenison-painting.php">Jenison Painting</a></li>
                                           <li id="menuDecks7"><a href="rockford-painting.php">Rockford Painting</a></li>
						<!--<li id="menuGutters"><a href="gutters.php">Gutter Cleaning</a></li>-->
					</ul>						
				</li>
				<li id="btnGallery"><a href="gallery.php">Gallery</a></li>
				<li id="btnEstimate"><a href="estimate.php">Estimate</a></li>
				<!--<li id="btnContact"  class="sub-menu sub-menu-1"><a href="#">Contact</a>
					<ul>
						<li class="sub-menu"><a href="contact.php">Send Us a Message</a></li>
						<li class="sub-menu"><a href="estimate.php">Request an Estimate</a></li>
					</ul>
				</li>-->											
			</ul>
		</div>
	</div>
</div>	
</div>	
</div>	
</div>	
</div>
<script type="text/javascript">
if (document.URL.indexOf('index.php') > -1) {
	$('#btnHome').addClass('active');
}
else if (document.URL.indexOf('about.php') > -1) {
	$('#btnAbout').addClass('active');
}
else if (document.URL.indexOf('interior.php') > -1) {
	$('#menuInterior').addClass('active');
	$('#btnServices').addClass('active');
}
else if (document.URL.indexOf('exterior.php') > -1) {
	$('#menuExterior').addClass('active');
	$('#btnServices').addClass('active');
}
else if (document.URL.indexOf('commercial.php') > -1) {
	$('#menuCommercial').addClass('active');
	$('#btnServices').addClass('active');
}
else if (document.URL.indexOf('decks.php') > -1) {
	$('#menuDecks').addClass('active');
	$('#btnServices').addClass('active');
}
else if (document.URL.indexOf('gutters.php') > -1) {
	$('#menuGutters').addClass('active');
	$('#btnServices').addClass('active');
}
else  if (document.URL.indexOf('gallery.php') > -1) {
	$('#btnGallery').addClass('active');
}
else if (document.URL.indexOf('estimate.php') > -1) {
	$('#btnEstimate').addClass('active');
}
else if (document.URL.indexOf('contact.php') > -1) {
	$('#btnContact').addClass('active');
}
</script>

