<div class="estimate1_wrapper">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="estimate1">
					<?php if (strpos($_SERVER["REQUEST_URI"] , "estimate") >0) : ?>
					<div class="row">
						<div class="span12">
							<center><div class="txt1" style="padding:90px 0px">A <strong>fair and straight-forward quote</strong> is just a few clicks away!</div></center>
						</div>
					</div>	
					<img src="images/home01.png" alt="" class="woman1">	
					<?php else : ?>
					<img src="images/man1.png" alt="" class="man1">	
					<div class="row">
						<div class="span8">
							<div class="txt1">Professional <b>painting services</b> is a great way to maintain your property and protect your investment!</div>	
						</div>
						<div class="span4">
							<div class="txt2"><a href="/estimate.php">Request Free Estimate</a></div>	
						</div>	
					</div>	
					<?php endif; ?>
				</div>	
			</div>	
		</div>
	</div>	
</div>

<div class="bot1_wrapper">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div class="bot1">
					<div class="row">
						<div class="span3 bottom-logo">
							<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo-tm.png" alt=""></a></div>
							<!--<div class="social_wrapper">
								<ul class="social clearfix">
									<li><a href="#"><img src="images/social_ic1.png"></a></li>
									<li><a href="#"><img src="images/social_ic2.png"></a></li>
									<li><a href="#"><img src="images/social_ic3.png"></a></li>	    
									<li><a href="#"><img src="images/social_ic4.png"></a></li>
									<li><a href="#"><img src="images/social_ic5.png"></a></li>
									<li><a href="#"><img src="images/social_ic6.png"></a></li>
								</ul>
							</div>-->
							<p style="margin-top: 12px;"><a href="#">Privacy Policy</a>&nbsp;&nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;&nbsp;<a href="#">Site Map</a></p>
						</div>	
						<div class="span9 block1" style="text-align:center;">
							<div class="bot1_title">West Michigan Areas We Serve</div>
							<p>
								<a href="http://www.adapainters.com">Ada</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.paintingcascade.com">Cascade</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.eastgrandrapidspainting.com">East Grand Rapids</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.paintingforesthills.com">Forest Hills</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.grandhavenpainting.com">Grand Haven</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.paintinggrandrapids.com">Grand Rapids</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.grandvillepainting.com">Grandville</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.paintingholland.com">Holland</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.jenisonpainting.com/">Jenison</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.kentwoodpainters.com">Kentwood</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.lowellpainting.com">Lowell</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.muskegonpainting.com">Muskegon</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.rockfordhomepainting.com">Rockford</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.springlakepainting.com">Spring Lake</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
								<a href="http://www.paintingwyoming.com">Wyoming</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
							</p>
							<p>Copyright   � 2014. All rights reserved.</p>
						</div>
					</div>	
				</div>	
			</div>	
		</div>	
	</div>
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>