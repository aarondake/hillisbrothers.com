<?php session_start();

    if (strip_tags(trim($_POST["captcha"])) <> $_SESSION['captcha_keystring'])
    {
        if (!isset($_SESSION['captchaTries'])) {
			// initialize captchaTries session variable, set it to 1
			$_SESSION['captchaTries'] = 1;
		} elseif ($_SESSION['captchaTries'] >= 10) {
			//if captcha tries is 10 times, return 'Captcha Fail', which will redirect page
			http_response_code(400);
			echo "Captcha Fail! Redirecting...";
			exit;
		} else {
			// increment captchaTries
			$_SESSION['captchaTries'] += 1;
		}
			
		http_response_code(400);
        echo "The Captcha value is not correct.  Please try again.";
        exit;
    }
    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $Form = strip_tags(trim($_POST["Form"]));
        $name = strip_tags(trim($_POST["Name"]));
        $email = filter_var(trim($_POST["Email"]), FILTER_SANITIZE_EMAIL);
		
        // Build the email content.
		$email_content = '';
		foreach($_POST as $item => $value){ 
			if ($item <> 'captcha')
				$email_content .= $item.': '.strip_tags(trim($value))."\r\n"; 
		}

		//$message = 'FROM: '.$name." \nEmail: ".$email."\nItems: \n".$items;
		//mail($to, $subject, $message, $headers);

        // Check that data was sent to the mailer.
        if ( empty($Form) OR empty($name) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "hillisnate@gmail.com";

        // Set the email subject.
        $subject = "'$Form' submission from $name";

       
        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Thank You! Your message has been sent.";
			//resets captcha tries (in case the user wants to attempt another form during the same browser session.  -ALD 11/12/14
			$_SESSION['captchaTries'] = 0;
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>