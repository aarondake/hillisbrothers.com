<!DOCTYPE html>
<html lang="en">
<head>
<title>Gallery | Painting Grand Rapids - Hillis Brothers Painting of West Michigan</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

<link rel="stylesheet" href="css/touchTouch.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/isotope.css" type="text/css" media="screen">      

<script type="text/javascript" src="js/touchTouch.jquery.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script>
$(document).ready(function() {
        /*var i = document.getElementById('liExterior');
        i.Click();*/
});
$(window).load(function() {
    // isotop
    var $container = $('#isotope-items'),
        $optionSet = $('#isotope-options'), 
        $optionSets = $('#isotope-filters'), 
        $optionLinks = $optionSets.find('a'); 
    $container.isotope({ 
        filter: '*',
        layoutMode: 'fitRows'
    });  
    $optionLinks.click(function(){ 
        var $this = $(this); 
        // don't proceed if already selected 
        if ( $this.hasClass('selected') ) { 
                return false; 
        }    		
        $optionSet.find('.selected').removeClass('selected'); 
        $this.addClass('selected');

        var selector = $(this).attr('data-filter'); 
        $container.isotope({ 
            filter: selector            
        }); 
        return false; 
    });    	
    $(window).on("resize", function( event ) {	
        $container.isotope('reLayout');		
    });		

    // touchTouch
    $('.thumb-isotope .thumbnail a').touchTouch();

}); //
</script>		

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
<div class="container">
<div class="row">
<div class="span12">
	
<h1>Our Photos</h1>

<div id="isotope-options">
    <ul id="isotope-filters" class="clearfix">
        <li><a href="#" data-filter="*" class="selected">Show All</a></li> 
        <li><a href="#" data-filter=".isotope-filter0" id="liExterior">Exteriors</a></li> 
        <li><a href="#" data-filter=".isotope-filter1">Interiors</a></li> 
        <li><a href="#" data-filter=".isotope-filter2">Decks</a></li> 	             
		<li><a href="#" data-filter=".isotope-filter3">Before/After</a></li> 	             
    </ul>            
</div>

<ul class="thumbnails" id="isotope-items">		 
    <?php 
        $myCategories=array (
            array("Exterior", "exterior"),
            array("Interior", "interior"),
            array("Deck", "deck"),
            array("Before & After","before-after")
        );
        $myPhotos=array(
            // EXTERIORS
            array("house-painters-siding-grand-rapids.jpg",
                "exterior-02.jpg",
                "cedar-staining-grand-rapids.jpg",
                "exterior-08.jpg",
                "exterior-05.jpg",
                "exterior-06.jpg",
                "exterior-07.jpg",
                "exterior-01.jpg",
                "exterior-09.jpg",
                "exterior-12.jpg",
                "exterior-11.jpg",
                "exterior-14.jpg",
                "exterior-08.jpg",
                "exterior-08.jpg"
            ),
            // INTERIORS
            array("kitchen-painting-grand-rapids.jpg",
                "interior-walls-refinish-grand-rapids.jpg",
                "interior-03.jpg",
                "interior-04.jpg",
                "interior-05.jpg",
                "interior-06.jpg",
                "interior-07.jpg",
                "interior-08.jpg",
                "interior-09.jpg",
                "interior-10.jpg",
                "interior-11.jpg",
                "interior-12.jpg"
            ),
            // DECKS
            array("deck-01.jpg",
                "deck-02.jpg",
                "deck-03.jpg",
                "deck-04.jpg",
                "deck-05.jpg",
                "deck-06.jpg",
                "deck-07.jpg",
                "deck-08.jpg",
                "deck-09.jpg",
            ),
            // BEFORE & AFTER
            array("before-01.jpg",
                "after-01.jpg",
                "before-02.jpg",
                "after-02.jpg",
                "before-03.jpg",
                "after-03.jpg",
                "before-after-04.jpg",
                "before-after-05.jpg"
            )
        );

        for ($i=0; $i<sizeof($myPhotos); $i++){
            for ($p=0; $p<sizeof($myPhotos[$i]); $p++) {?>
                <li class="span4 isotope-element isotope-filter<?=$i;?>">
                    <div class="thumb-isotope">
                        <div class="thumbnail clearfix">
                            <a href="images/exterior/<?=$myPhotos[$i][$p];?>">
                                <figure>
                                    <img src="images/<?=$myCategories[$i][1]."/".$myPhotos[$i][$p];?>" alt=""><em></em>
                                </figure>
                                <div class="caption">							
                                   #<?=$myCategories[$i][0];?>
                                </div>
                            </a>				
                        </div>
                    </div>
                </li>
            <?php
            } 
        } ?>
            
</ul>

    
</div>	
</div>	
</div>	
</div>

<?php include('footer.php'); ?>