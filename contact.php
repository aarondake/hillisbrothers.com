<!DOCTYPE html>
<html lang="en">
<head>
<title>contacts</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

<script type="text/javascript" src="js/cform.js"></script>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
<div class="container">
<div class="row">
<div class="span6">

<h2>Contact Form</h2>

<?php
include 'contact-form.php';
?>

</div>
<div class="span6">

<h1>Areas We Serve</h1>	
<img src="http://new.paintinggrandrapids.com/images/map.png" />
<!--<figure class="google_map">
    <a target="_blank" href="https://maps.google.com/maps/ms?hl=en&ie=UTF8&msa=0&msid=105799366203833083134.0004583b0b8c0aaa3d019&ll=42.949135,-85.648041&spn=0.228681,0.528717&z=12&dg=feature"><img src="/images/google-maps.jpg"/></a>
</figure>-->

</div>

</div>	
</div>	
</div>

<?php include('footer.php'); ?>