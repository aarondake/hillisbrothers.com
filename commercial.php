<!DOCTYPE html>
<html lang="en">
<head>
<title>Welcome to Hillisbrother's Commercial Painters</title>
<meta charset="utf-8">
<meta name="description" content="For Commercial painting services for your residence or business, look no further than Hillisbrother's Commercial Painters. As a leading professional Commercial painting company.">
<meta name="keywords" content="Commercial painters, Commercial painting">
<meta name="author" content="Hillis Brothers Painting">
<meta name="allow-search" content="yes" />
<meta name="revisit-after" content="2 days" >

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<h1>Commercial Painting</h1>
			<div class="span8">
				<div class="thumb2">
					<img style="float:right; margin:15px;" src="http://paintinggrandrapids.com/Assets/Comm2.jpg" />
					<p>When your common areas need a fresh look, Hillis Brothers is the commercial painter to call. With Hillis Brothers, you don't need to worry about your other surfaces. We take great care to remove, cover, and protect all areas during your project. Additionally, Hillis Brothers will prepare the surface to be painted to insure the best final outcome. Hillis Brothers will complete all necessary sanding, restoration and repairs before the painting project begins. </p>
					<p>The final quality of a job comes primarily from the prep work. Contaminants such as dirt, mold and mildew, car exhaust, and loose dirt can affect the adhesion of the paint and overall quality of the exterior painting job. A thorough pressure washing removes most of the problem areas. Scraping, performing minor repairs, caulking around joints and trim insures your exterior paint will look good and last for years.</p>
					<p>For more information about our Commercial Painting services, call us at  <strong>616-554-5140</strong> or visit our <a href="contact.html">contact</a> page.</p>
				</div>
			</div>
			<div class="span4">
				<h4>Interior</h4>
				<ul>
					<li>Tilt up concrete</li>
					<li>Drywall/Plaster</li>
					<li>Crown Molding</li>
					<li>Trim</li>
					<li>Metal/Wood Doors</li>
					<li>Block/Brick Substrates</li>
					<li>Structural Steel</li>
					<li>Ceilings</li>
				</ul>
				<h4>Exterior</h4>
				<ul>
					<li>Service Doors/Overhead doors</li>
					<li>All Metal Surfaces</li>
					<li>Cedar Siding, Wood Decks</li>
					<li>Steel Staircases &amp; Balconies</li>
					<li>Brick Cinder Block</li>
					<li>Wrought Iron Fences</li>
					<li>Roof Coping</li>
					<li>Aluminum Siding</li>
				</ul>
			</div>
		</div>	
	</div>	
</div>

<?php include('footer.php'); ?>