<!DOCTYPE html>
<html lang="en">
<head>
<title>services</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
<div class="container">
<div class="row">
<div class="span8">

<h1>Services Overview</h1>	
	
<div class="thumb3">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/services01.jpg" alt=""></figure>
		<div class="caption">
			<h4>Integer lacinia, velit sit amet bibendum hendrerit neque justo ornare urna, non sodal</h4>
			<p>
				Vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. Aliquam 
			</p>
			<a href="#" class="button1">Read more</a>
		</div>
	</div>
</div>

<div class="thumb3">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/services02.jpg" alt=""></figure>
		<div class="caption">
			<h4>Amet bibendum hendrerit neque justo ornare urna, non sodales nunc nibh vel purus. In </h4>
			<p>
				Vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. Aliquam 
			</p>
			<a href="#" class="button1">Read more</a>
		</div>
	</div>
</div>

<div class="thumb3">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/services03.jpg" alt=""></figure>
		<div class="caption">
			<h4>Relit sit amet bibendum hendrerit neque justo ornare urna, non sodales nunc nibh vel </h4>
			<p>
				Vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. Aliquam 
			</p>
			<a href="#" class="button1">Read more</a>
		</div>
	</div>
</div>

<h2>Special Offers</h2>

<p>
	Enim quis ultricies gravida, mauris libero hendrerit lorem, vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. 
</p>

<br>

<div class="thumb3">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/services04.jpg" alt=""></figure>
		<div class="caption">
			<h4>Integer lacinia, velit sit ibendum que justo ornare urna, non sodales nunc</h4>
			<p>
				Sodales, enim quis ultricies gravida, mauris libero hendrerit lorem, vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. Aliquam lobortis. Nunc dignissim pharetra urna. Mauris Sed sodales, enim quis ultricies gravida, mauris libero hendrerit lorem, vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. Nulla in pede id dui consectetuer vulputate. Aliquam lobortis. Nunc dignissim pharetra urna. Mauris scelerisque augue eu felis. Nunc quam massa, mattis quis, hendrerit sed, mattis eu, est. Vivamus pretium metus ut nisl. Nullam aliquet mauris eu neque.  
			</p>
			<a href="#" class="button1">Read more</a>
		</div>
	</div>
</div>





</div>
<div class="span4">
	
<h3 class="c3">Services List</h3>

<div class="box3">
<div class="box3_inner">
	
<ul class="ul1">
  <li><a href="#">Lorem ipsum dolor hendrerit sed</a></li>
  <li><a href="#">Sit amet consectetue sed sodales, enim quis ultricies</a></li>
  <li><a href="#">Adipiscing elit</a></li>
  <li><a href="#">Nunc suscipit</a></li>
  <li><a href="#">Suspendisse enim arcu</a></li>  
  <li><a href="#">Convallis non cursus sed</a></li>
  <li><a href="#">Dignissim et est</a></li>
  <li><a href="#">Aenean semper aliquet libero</a></li>  	            		      	      			      
</ul>

</div>	
</div>

<h2>Usful Info</h2>

<h4>Non sodales nunc nibh vel purus. In urna. Sed blandit ligula dolor, ut </h4>

<p>
	Sed sodales, enim quis ultricies gravida, mauris libero hendrerit lorem, vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam.
</p>

<ul class="ul2">
  <li><a href="#">Lorem ipsum dolor hendrerit sed</a></li>
  <li><a href="#">Sit amet consectetue sed sodales, enim quis ultricies</a></li>
  <li><a href="#">Suspendisse enim arcu</a></li>  
  <li><a href="#">Convallis non cursus sed</a></li> 	            		      	      			      
</ul>

<br>

<p>
	Mauris libero hendrerit lorem, vel vulputate urna lacus quis mauris. Duis aliquam. Vivamus ac ligula. Cras et metus a ipsum suscipit aliquam. Morbi at justo. 
</p>









</div>
</div>	
</div>	
</div>

<div class="estimate1_wrapper">
<div class="container">
<div class="row">
<div class="span12">
<div class="estimate1">
<img src="images/man1.png" alt="" class="man1">	
<div class="row">
<div class="span8">
<div class="txt1">Professional <b>painting services</b> is a great way to maintain your property and protect your investment!</div>	
</div>
<div class="span4">
<div class="txt2"><a href="#">Request Free Estimate</a></div>	
</div>	
</div>	
</div>	
</div>	
</div>
</div>	
</div>

<div class="bot1_wrapper">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1">
<div class="row">
<div class="span3">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/logo2.png" alt=""></a></div>

<div class="social_wrapper">
	<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
	    <li><a href="#"><img src="images/social_ic5.png"></a></li>
	    <li><a href="#"><img src="images/social_ic6.png"></a></li>
	</ul>
</div>

<footer><div class="copyright">Copyright   © 2013. All rights reserved.<br><a href="#">Privacy Policy</a>&nbsp;&nbsp;&nbsp;<span>|</span>&nbsp;&nbsp;&nbsp;<a href="#">Site Map</a></div></footer>



</div>	
<div class="span3 block1">
	
<div class="bot1_title">Contacts Details</div>

8901 Marmora Road,<br>
Glasgow, D04 89GR.<br>
Telephone: +1 800 123 1234<br>
Fax: +1 800 123 1234<br>
E-mail: <a href="#">mail@bteamny.com</a>


</div>
<div class="span3 block2">
	
<div class="bot1_title">why choose us</div>

In pharetra ipsum condimentum elit. Suspendisse mattis turpis eget odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.


</div>
<div class="span3 block3">
	
<div class="bot1_title">Website search</div>

<div class="search-form-wrapper clearfix">
<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	
</div>


</div>
</div>	
</div>	
</div>	
</div>	
</div>	
</div>


	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>