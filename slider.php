
<div id="slider_wrapper">
<div id="slider_inner">
<div class="container">
<div class="row">
<div class="span12">
<div id="slider" class="clearfix">
<div id="camera_wrap">
<div data-src="images/painting-grand-rapids.jpg">
	
	<div class="camera_caption fadeIn clearfix">
		<div class="txt1"><a href="about.php" class="button0"></a></div>
			<div class="caption">
				<div class="txt2">Professional</div>
				<div class="txt3">Painting of Grand Rapids</div>
			</div>								
		</div>     
	</div>
	
	<div data-src="images/exterior/house-painting-exterior-grand-rapids.jpg">
		<div class="camera_caption fadeIn clearfix">
			<div class="txt1"><a href="exterior.php" class="button0"></a></div>
			<div class="caption">
				<div class="txt2">Professional</div>
				<div class="txt3">Exterior House Painting</div>
			</div>								
		</div>     
	</div>
	
	<div data-src="images/slide05.jpg">
		<div class="camera_caption fadeIn clearfix">
			<div class="txt1"><a href="interior.php" class="button0"></a></div>
			<div class="caption">
				<div class="txt2">Professional</div>
				<div class="txt3">Interior Room Painting</div>
			</div>								
		</div>     
	</div>

	<div data-src="images/deck-staining-grand-rapids(2).jpg">
		<div class="camera_caption fadeIn clearfix">
			<div class="txt1"><a href="decks.php" class="button0"></a></div>
			<div class="caption">
				<div class="txt2">Professional</div>
				<div class="txt3">Deck Resurfacing</div>
			</div>								
		</div>     
	</div>
	
	<div data-src="images/slide03.jpg">
		<div class="camera_caption fadeIn clearfix">
			<div class="txt1"><a href="estimate.php" class="button0"></a></div>
			<div class="caption">
				<div class="txt2">Request A</div>
				<div class="txt3">Free Professional Estimate</div>
			</div>								
		</div>     
	</div>
	

</div>	
</div>	
</div>	
</div>	
</div>	
</div>	
</div>