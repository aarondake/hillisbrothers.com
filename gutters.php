<!DOCTYPE html>
<html lang="en">
<head>
<title>Gutters</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				
				<h1>GUTTERS CLEANED for as low as $50!</h1>

				<div class="row">
					<div class="span6">
							<h2>How to Clean Gutters</h2>
							<ol>
							<li>Gain access to the roof with a ladder. Don't lean the ladder against a downspout or gutter, which can easily bend or break.</li>

								<li>Remove leaves and twigs from gutters by hand or with a large spoon, a gutter scoop or a small garden trowel.</li>

								<li>Wet down caked-on dirt, which can be difficult to remove without damaging the gutter itself, then remove it with the gutter scoop or trowel.</li>

								<li>Put debris in a bucket or plastic trash bag placed on the roof or ladder. If you use a bag, you can just drop it when it's full.</li>

								<li>Check that the downspouts aren't clogged. Use water to unclog your downspouts by placing a garden hose in the opening. But be gentle at first; downspouts aren't designed to withstand the same pressure as a house drain. If a plugged downspout can't be cleared with a hose, use a small plumber's snake or an unbent clothes hanger. Again, be gentle. Gutters are not as strong as house pipes.</li>

								<li>Alternatively, use a leaf blower to clean the gutters; however, remember that you'll be high up, often in awkward postures, and carrying a good-sized machine that not only is awkward to use but also can blind you with dust.</li>

								<li>Use the hose to flush the gutters with water after cleaning. (This is also the best time to find out if there are any leaks in the system.) </li>
							</ol>
							
					</div>		
					<div class="span6">
						<div class="thumb2">
							<img style="max-width:500px;" src="/images/gutters/dirty-gutters.jpg" />
						</div>
						<div class="thumb2">
							<img style="max-width:500px;" src="/images/gutters/before-after.jpg" /></p>
						</div>
					</div>		
				</div>	
			</div>	
		</div>	
	</div>	
</div>

<?php include('footer.php'); ?>