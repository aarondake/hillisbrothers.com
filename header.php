<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="google-site-verification" content="-srFyqeqPPJAFIRGo-FMQ6qQ3J4lKXgaYLvooJ5QZSo" />
<meta name="msvalidate.01" content="909388633F04AA1DD66AF165284FC761" />
<meta name="category" content="Painters">
<meta name="classification" content="Paintings"/>
<meta name="googlebot" content="index, follow" />
<meta name="msnbot" content="index, follow" />
<meta name="YahooSeeker" content="index, follow" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<!--	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<!--[if lt IE 8]>s
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->

<!-- Google Analytics -->
<?php include_once("analyticstracking.php") ?>