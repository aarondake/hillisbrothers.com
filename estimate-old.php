<!DOCTYPE html>
<html lang="en">
<head>
<title>Estimate</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
			<center>
				<h1>Request An Estimate. <br/><small>2 ways...take your pick!</small></h1>
			</center>
			</div>
		</div>
		<div class="row">
			<div class="span6">
			<center>
			<h3>1. Quickly and efficiently online.</h3>
			<p>Take your own measurements and fill out a quick online form!</p>
			<button type="button" class="btn btn-primary btn-large" onclick="document.getElementById('OnlineEstimate').style.display='block';document.getElementById('RequestAppt').style.display='none';">This is for me!</button>
			</div>
			</center>
			<div class="span6">
				<center>
				<h3>2. Schedule an on-site appointment.</h3>
				<p>We do all the work! &nbsp;Pick the day of your choosing!</p>
				<button type="button" class="btn btn-primary btn-large" onclick="document.getElementById('RequestAppt').style.display='block';document.getElementById('OnlineEstimate').style.display='none';">I prefer this type!</button>
				</center>
			</div>
		</div> <!--ROW-->
		<div class="row">
			<div class="span12">
			<h1><!--Make some dotty lines!--></h1>
			</div>
		</div>
		
		
		<div class="row fade-in" id="OnlineEstimate" style="">
			<div class="span12">
				<h3 class="c3">Online Estimate Form</h3>
				<p>If your project is straightforward and you would like to skip the entire step of an on-site appointment, use a tape measure to collect your measurements and get your project in motion that much quicker! </p>
				<p>Just select which type of project below, and fill out the corresponding form.</p>
				<div class="container">
					<div id="content">
						<ul id="tabs" class="nav nav-tabs big-tabs" data-tabs="tabs">
							<li class="active"><a href="#ext" data-toggle="tab">Exterior</a></li>
							<li><a href="#int" data-toggle="tab">Interior</a></li>
							<li><a href="#comm" data-toggle="tab">Commercial</a></li>
							<li><a href="#deck" data-toggle="tab">Deck</a></li>
						</ul>
						<div id="my-tab-content" class="tab-content">
							<div class="tab-pane active" id="ext">
								<h3>Exterior Estimate Form</h3>
								<?php include('estimate-exterior.html'); ?>
							</div>
							<div class="tab-pane" id="int">
								<h3>Interior Estimate Form</h3>
								<?php include('estimate-interior.html'); ?>
							</div>
							<div class="tab-pane" id="comm">
								<h3>Commercial Estimate Form</h3>
								<?php include('estimate-commercial.html'); ?>
							</div>
							<div class="tab-pane" id="deck">
								<h3>Deck Estimate Form</h3>
								<?php include('estimate-deck.html'); ?>
							</div>
						</div>
					</div>
					 
					 
					<script type="text/javascript">
						jQuery(document).ready(function ($) {
							$('#tabs').tab();
						});
					</script>    
				</div> <!-- container -->
			</div>
		</div>
		<div class="row" id="RequestAppt" style="//display:none;">
			<div class="span12">
				<h3 class="c3">On-Site Appointment</h3>
				<p>Not interested in taking your own measurements? No problem! Use this calendar to schedule a free on-site appointment with one of our expert painters!  You will receive a quote right there on the spot!</p>
				<?php include 'freshbooks2.html'; ?>
				
				<p>If you prefer to talk to us first, just call <strong>616-554-5140</strong> or visit our <a href="contact.php">Contact Page</a>.</p>
				<p>Thanks!</p>
				<p>-The Hillis Brothers</p>
			</div>
				<!--<div class="box3">
					<div class="box3_inner">
						<p><a href="#" onClick="alert(document.URL);">
						<img src="/images/form_button_int_up.gif" onmouseover="this.src='/images/form_button_int_over.gif'" onmouseout="this.src='/images/form_button_int_up.gif'"></a></p>
						
						<p><a href="#" onClick="alert(document.URL);">
						<img src="/images/form_button_ext_up.gif" onmouseover="this.src='/images/form_button_ext_over.gif'" onmouseout="this.src='/images/form_button_ext_up.gif'"></a></p>						
						<p><a href="#" onClick="alert(document.URL);">
						<img src="/images/form_button_comm_up.gif" onmouseover="this.src='/images/form_button_comm_over.gif'" onmouseout="this.src='/images/form_button_comm_up.gif'"></a></p>
						
						<p><a href="#" onClick="alert(document.URL);">
						<img src="/images/form_button_deck_up.gif" onmouseover="this.src='/images/form_button_deck_over.gif'" onmouseout="this.src='/images/form_button_deck_up.gif'"></a></p>
						
						<p><a href="#" onClick="alert(document.URL);">
						<img src="/images/form_button_gutter_up.gif" onmouseover="this.src='/images/form_button_gutter_over.gif'" onmouseout="this.src='/images/form_button_gutter_up.gif'"></a></p>
					</div>	
				</div>-->
				

				
			</div>
		</div>	
	</div>
</div>

<?php include('footer.php'); ?>