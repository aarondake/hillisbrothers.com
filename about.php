<!DOCTYPE html>
<html lang="en">
<head>
<title>About | Painting Grand Rapids - Hillis Brothers Painting of West Michigan</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				
				<h1>Proudly served 30,000 customers since 1992!</h1>

				<div class="row">
					<div class="span4">

						<div class="thumb2">
							<div class="thumbnail clearfix">
								<figure class="img-polaroid"><img src="images/the-brothers.png" alt=""></figure>
								<div class="caption">
									
								</div>
							</div>
						</div>

						</div>
					<div class="span8">

						<h4>20 years of knowledge and experience back every project we do!  </h4>

						<div class="row">
							<div class="span4">
								<p>Hillis Brothers Painting is based out of Grand Rapids, but provides service to <strong>Big Rapids</strong>, <strong>Muskegon</strong>, <strong>Holland</strong>, <strong>Kalamazoo</strong> and everything in between.  Over the years, we have grown from a small two-person team to a full-service restoration business, operating many crews and serving hundreds of satisfied customers every year.  Along the way, our areas of expertise have also grown and now include speciality projects such as deck restoration, cabinet refinishing, and wall repair.</p>
							</div>
							<div class="span4">
								<p>We are fully licensed, insured, and use only the highest quality paints on professionally prepared surfaces to insure an enduring finish that will last for years. We always implement the latest and most effective techniques, using a wide variety of paints including 100% acrylic latex, oil based, and enamel to meet the particular demands of any job. Visit the Services page to read more and don't hesitate to schedule and estimate for a free, no-hassle quote for your project today!</p>
								<a href="/testimonials.php" class="button1">View Testimonials</a>
							</div>	
						</div>
					</div>	
				</div>
			</div>	
		</div>
		


	</div>	
</div>

<?php include('footer.php'); ?>