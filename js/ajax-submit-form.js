$(function() {

	// Get the form.
	var form = $('#ajax-contact-form');

	// Get the messages div.
	var formMessages = $('#form-messages');

	// Set up an event listener for the contact form.
	$(form).submit(function(e) {
		// Stop the browser from submitting the form.
		e.preventDefault();
		
		// Get the Captcha
		var captcha = $('captcha');
		
		// Serialize the form data.
		var formData = $(form).serialize();

		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response) {
			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('form-failure');
			$(formMessages).addClass('form-success');

			// Set the message text.
			$(formMessages).text(response);
			
			// Hide the form
			$('#ajax-contact-form').css("display","none");
		})
		.fail(function(data) {
						
			// Set the message text.
			if (data.responseText !== '') {
				$(formMessages).text(data.responseText);
			} else {
				$(formMessages).text('Oops! An error occured and your message could not be sent.');
			}
			
			// Make sure that the formMessages div has the 'error' class.
			$(formMessages).removeClass('form-success');
			$(formMessages).addClass('form-failure');
			
			if (data.responseText == 'Captcha Fail! Redirecting...') {
				//captcha failed, wait 3 seconds then redirect user.
				var start = new Date().getTime();
				var count = 0;
				for (var i = 0; i < 1e7; i++) {
					count = new Date().getTime() - start;
					if (count > 3000){
						break;
					}
				}
			    window.location = "http://www.google.com/";
			}
				
			// Clear existing captcha value
			$('#inputCaptcha').val('');
			// Refresh the Captcha
			$('#imgCaptcha').attr("src","captcha/captcha.php?v=".concat(Math.random()).toString());
		});
	});

});
