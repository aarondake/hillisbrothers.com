<div id="note"></div>
	<div id="form-messages">This is a the message that the user will receive!</div>
	<div id="fields">
	<form id="ajax-contact-form" class="form-horizontal" method="post" action="mailer.php">
	<input type="hidden" name="Form" value="Contact Form" />
		<div class="field">
			<label for="inputName">Name:</label>
			<input type="text" id="inputName" name="Name" placeholder="Your name:" required />
		</div>
		<div class="field">
			<label for="inputEmail">Email:</label>
			<input type="email" id="inputEmail" name="Email" placeholder="Your email:" required />
		</div>
		<div class="field">
			<label for="inputPhone">Phone:</label>
			<input type="text" id="inputPhone" name="Phone" placeholder="Your phone number (optional):" />
		</div>
		<div class="field">
			<label for="inputMessage">Message:</label>
			<textarea id="inputMessage" name="Message" placeholder="Your message:" required></textarea>
		</div>
			
		<div class="field">
			<label for="inputCaptcha">Captcha:</label>			      
			 <input class="captcha-text" type="text" id="inputCaptcha" name="captcha" placeholder="Captcha:" required/>
			 <img id="imgCaptcha" src="captcha/captcha.php">
		</div>	
		<div class="field">
			<button type="submit" class="submit">submit</button>
		</div>
	</form>
</div>

<script src="js/ajax-submit-form.js"></script>