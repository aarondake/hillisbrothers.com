<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>House Painting Contractors and Painters - Hillisbrothers.com</title>
<meta charset="utf-8">
<meta name="description" content="Hillisbrother's house painting contractors and painters specialize in both exterior & interior house painting because our expert painters are highly trained.">
<meta name="keywords" content="House painters ,House Painting Contractors ">
<meta name="author" content="Hillis Brothers Painting">

<?php include('header.php'); ?>

<link rel="stylesheet" href="css/camera.css" type="text/css" media="screen">

<script type="text/javascript" src="js/camera.js"></script>
<script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
<script>
$(document).ready(function() {
	// camera
	$('#camera_wrap').camera({
		//thumbnails: true
		autoAdvance			: true,		
		mobileAutoAdvance	: true,
		//fx					: 'simpleFade',
		height: '40%',
		hover: false,
		loader: 'none',
		navigation: false,
		navigationHover: true,
		mobileNavHover: true,
		playPause: false,
		pauseOnClick: false,
		pagination			: true,
		time: 3000,
		transPeriod: 1000,
		minHeight: '200px'
	});



}); //
$(window).load(function() {
	//

}); //
</script>		

</head>

<body class="main">
<?php 
include 'top.php';
include 'menu.php';
include 'slider.php';
?>

<div class="shadow1">
	<div class="container">
		<div class="row">
			<div class="span12 text-center sub-scroller-text">
				<img src="images/shadow1.png" alt="" class="img">	
				<h1><em><?="Residential & Commercial Painting in Grand Rapids, Michigan!" ?></em></h2>
			</div>	
		</div>	
	</div>	
</div>


<div class="banners">
	<div class="container">
		<div class="row">
			<div class="span4 banner banner1">
				
				<div class="banner_inner">
					
					<div class="txt1"><img src="images/interior-painting.png" title="Interior Painting" alt="Interior Painting" class="img">Interior</div>

					<div class="txt2"><ul><li>Ceilings, Walls, and Trim</li><li>Low VOC products available</li><li>Wall Repair</li><li>Cabinet Painting and Refinishing</li><li>Wallpaper Removal</li><li>Color Consultation With Designer</li></ul></div>

					<div class="txt3"><a href="/interior.php">learn more</a></div>
				</a>
				</div>	
			</div>
			<div class="span4 banner banner2">
				
				<div class="banner_inner">
					<div class="txt1"><img src="images/exterior-painting.png" title="Exterior Painting" alt="Exterior Painting" class="img">Exterior</div>

					<div class="txt2"><ul><li>We paint all types of surfaces including Cedar, Vinyl, Aluminum, Masonry, and Hardwood</li><li>Wood Repairs</li><li>Color Consultation with Designer</li><li>Two-Year Warranty</li></ul></div>

					<div class="txt3"><a href="/exterior.php">learn more</a></div>

				</div>			

			</div>
			<div class="span4 banner banner3">
				<div class="banner_inner">
					<div style="display:table; width:100%">
						<div style="display:table-row">
							<div style="display:table-cell">
								<img src="images/deck-restoration.png" title="Deck Restoration " alt="Deck Restoration" class="img">
							</div>
							<div style="display:table-cell; vertical-align: middle;">
								<div class="txt1">Deck<br/>Restoration</div>
							</div>
						</div>
					</div>

					<div class="txt2"><ul><li>Mildew Treatments</li><li>Power Washing</li><li>Board Repair</li><li>Semi Transparent and Solid Stains</li><li>Composite Coatings</li><li>Up to Three Year Warranty</li></ul></div>

					<div class="txt3"><a href="/decks.php">learn more</a></div> 

				</div>	
			</div>	
		</div>	
		
		<div class="row">
			<div class="span12">
				<h2>Current Promotions</h2>
			</div>	
		</div>	

		<div class="row">
			<div class="span6">
				<a href="/images/promotions/grand-rapids-painting-front-2015.jpg" target="_blank"><img src="/images/promotions/grand-rapids-painting-front-small-2015.jpg"></a>
			</div>
			<div class="span6">	
				<a href="/images/promotions/grand-rapids-painting-back-2015.jpg" target="_blank"><img src="/images/promotions/grand-rapids-painting-back-small-2015.jpg"></a>
			</div>
		</div>	
	</div>	
</div>

<script type="text/javascript" src="//sites.yext.com/154214-posts.js"></script>


<div id="content">
	<div class="container">
	<div class="row">
	<div class="span8">

		<h2 class="c1">Drop us a line!</h2>	

		<div style="width:200px;float:left;">
			<figure ><img src="images/home01.png" alt=""></figure>
		</div>
		<div style="min-width:200px;width:auto;overflow:hidden;">
		
		<?php
include 'contact-form.php';
?>

		</div>
	</div>	
	<div class="span4">
		
	

	<div class="box3_inner">
		<h2 class="c3">Other jobs we do...</h2>
	<ul class="ul1">
	  <li>Wallpaper Removal</li>
	  <li>Minor Wall and Floor Repair</li>
	  <li>Hardwood Floor Resurfacing</li>
	  <li>Garage Floor Painting and Texturing</li>
	  <li>Residential and Industrial Pressure Cleaning</li>
	  <li>Cedar Siding Repair and Staining</li>
	  <li>Aluminum and Vinyl Siding Resurfacing</li>
	</ul>
	
	</div>




	</div>
	</div>	
	</div>	
</div>

<?php include('footer.php'); ?>