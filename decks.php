<!DOCTYPE html>
<html lang="en">
<head>
<title>Deck staining | Deck refinishing services</title>
<meta charset="utf-8">
<meta name="description" content="Most companies use the same approach to maintaining your deck like Deck staining and Deck refinishing. More about our deck staining and refinishing services, call @ 616-554-5140">
<meta name="keywords" content="Deck staining, Deck refinishing">
<meta name="author" content="Hillis Brothers Painting">
<meta name="allow-search" content="yes" />
<meta name="revisit-after" content="2 days" >

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				
				<h1>Decks</h1>

				<div class="row">
					<div class="span6">

						<div class="thumb2">
							<img class="img-responsive" style="float:right; margin:15px;" src="http://paintinggrandrapids.com/Assets/Deck1.jpg" />
							<p>Most companies use the same approach when it comes to maintaining your deck. First, they wash and treat with a bleach to remove the mildew and mold. Then a sealant or stain is used to preserve the wood. The Hillis Brothers difference uses a floor sander to take off any layers of stain that might prevent the new stain from soaking in.</p>

							<p>At Hillis Brothers Painting, we have found through experience that a systematic approach helps us to deliver the quality job that we have promised you.  See our process on this page for the general steps we will take on your project.  Your situation may vary depending upon your needs.</p>

							<p>For more information about our deck restoration services, call us at 616-554-5140 or visit our contact page.</p>

						</div>

					</div>
					<div class="span6">
						<img class="img-responsive" src="http://paintinggrandrapids.com/Assets/deck2.jpg" style="float:right;">
						<h3>Our Process</h3>
						<p>Hillis Brothers Painting has performed countless deck resurfaces for homes all across West Michigan. We have a proven 3-step deck restoration that is guaranteed to have your old deck looking new again. The three steps include:</p>
						<p style="font-size:22px">
							<ul>
								<li>Mildew treating/Pressure washing</li>
								<li>Floor sanding</li>
								<li>Staining</li>
							</ul>
						</p>
					</div>	
				</div>
			</div>		
		</div>	
	</div>	
</div>

<?php include('footer.php'); ?>