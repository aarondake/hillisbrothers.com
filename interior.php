<!DOCTYPE html>
<html lang="en">
<head>
<title>Interior painting services is a great way to maintain your property</title>
<meta charset="utf-8">
<meta name="description" content="Painting interior walls is the easiest way to freshen up your place. When you have an interior painting job that needs to be done, Hillis Brothers Painting is here to help!">
<meta name="keywords" content="Interior painting, Interior painting Services">
<meta name="author" content="Hillis Brothers Painting">

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				
				<h1>Interior Painting</h1>

				<div class="row">
					<div class="span6">

						<div class="thumb2">
							<img style="float:left; margin:15px;" src="http://paintinggrandrapids.com/Assets/int1.jpg" />
							<p>When you have an interior painting job that needs to be done, Hillis Brothers Painting is here to help! When we're finished, your home will look just as clean as it did when we started, and it will have a fine, enduring finish that will last for years to come. Our company looks for ways to save you money, and we always do the job right the first time. We'll work hard to solve any issues that may come up, and we are always professional and easy to work with. With all our care and attention to detail, Hillis Brothers is the perfect choice for all your painting needs! A fresh coat of paint is one of the most creative and inexpensive ways to redecorate a room. Selecting a new interior paint color can give your living space the perfect look and feel for any environment. At Hillis Brothers, we'll work closely with you to insure your end result will be a warm, inviting home that you'll enjoy for a long time! </p>
						</div>

					</div>
					<div class="span6">
						<h3>Our Process</h3>
						<img src="http://paintinggrandrapids.com/Assets/int2.jpg" style="float:right;">
						<p>At Hillis Brothers Painting we have found through experience that a systematic approach helps us to deliver the quality job that we have promised you.  Below are the general steps we will take on your project. Your situation may vary depending upon your needs.</p>
						<p style="font-size:22px">
							<ul>
								<li>Set-up</li>
								<li>Surface Preparation</li>
								<li>Paint</li>
								<li>Clean</li>
								<li>Inspect</li>
							</ul>
						</p>
					</div>	
				</div>
			</div>		
		</div>	
	</div>	
</div>

<?php include('footer.php'); ?>