<!DOCTYPE html>
<html lang="en">
<head>
<title>Exterior painting - Discover inspiration for your exterior design</title>
<meta charset="utf-8">
<meta name="description" content="Give your house a fresh, updated look with exterior paint. Maintaining your property's exterior adds value to your home.">
<meta name="keywords" content="Exterior painting">
<meta name="author" content="Hillis Brothers Painting">
<meta name="allow-search" content="yes" />
<meta name="revisit-after" content="2 days" >

<?php include('header.php'); ?>

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				
				<h1>Exterior Painting</h1>

				<div class="row">
					<div class="span6">

						<div class="thumb2">
							<img style="float:right; margin:15px;" src="http://paintinggrandrapids.com/Assets/Ext1.jpg" />
							<p>Hillis Brothers Painting handles the demands of today's exterior job sites, offering power washing, deck resurfacing, painting, and gutter installation and repair for residential, commercial, industrial, retail, and churches. Our professional estimating, project management, and production staff is your guarantee that the project will be completed on time, on budget and with uncompromising quality.</p>

							<p>We support the needs of today's exterior homes and businesses by supplying knowledgeable manpower, quality products, and tried and true methods. All of our workers are trained in proper safety and client service protocol, to ensure your project is handled with uncompromised professionalism. Our mission is to be the best provider of painting, rain gutters, and special coatings for all of our residential and commercial customers.</p>

							<p>Maintaining your property's exterior adds value to your home or business in several ways. A clean and well-maintained exterior increases curb appeal and increases the value of your home or business while reinforcing a positive image.</p>

						</div>

					</div>
					<div class="span6">
						<h3>Our Process</h3>
						<img src="http://paintinggrandrapids.com/Assets/ext2.jpg" style="float:right;">
						<p>At Hillis Brothers Painting we have found through experience that a systematic approach helps us to deliver the quality job that we have promised you.  Below are the general steps we will take on your project. Your situation may vary depending upon your needs.</p>
						<p style="font-size:22px">
							<ul>
								<li>Wash</li>
								<li>Surface Preparation</li>
								<li>Paint</li>
								<li>Clean</li>
								<li>Inspect</li>
							</ul>
						</p>
						<p style="clear:both; margin-top:30px;">For more information about our exterior services, call us at 616-554-5140 or visit our contact page.</p>
					</div>	
				</div>
			</div>		
		</div>	
	</div>	
</div>

<?php include('footer.php'); ?>