<!DOCTYPE html>
<html lang="en">
<head>
<title>Testimonials | Painting Grand Rapids - Hillis Brothers Painting of West Michigan</title>
<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<?php include('header.php'); ?>

<link rel="stylesheet" href="css/touchTouch.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/isotope.css" type="text/css" media="screen">      

<script type="text/javascript" src="js/touchTouch.jquery.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script>
$(window).load(function() {
    // isotop
    var $container = $('#isotope-items'),
        $optionSet = $('#isotope-options'), 
        $optionSets = $('#isotope-filters'), 
        $optionLinks = $optionSets.find('a'); 
    $container.isotope({ 
        filter: '*',
        layoutMode: 'fitRows'
    });  
    $optionLinks.click(function(){ 
        var $this = $(this); 
        // don't proceed if already selected 
        if ( $this.hasClass('selected') ) { 
            return false; 
        }    		
        $optionSet.find('.selected').removeClass('selected'); 
        $this.addClass('selected');

        var selector = $(this).attr('data-filter'); 
        $container.isotope({ 
            filter: selector            
        }); 
        return false; 
    }); 

    $(window).on("resize", function( event ) {	
            $container.isotope('reLayout');		
    });		

    // touchTouch
    $('.thumb-isotope .thumbnail a').touchTouch();

}); //
</script>		

</head>

<body class="subpage">

<?php 
include 'top.php'; 
include 'menu.php';
include 'breadcrumbs.php';
?>

<div id="content">
<div class="container">
<div class="row">
<div class="span12">
	
<h1>Testimonials</h1>

<ul class="thumbnails" id="isotope-items">		 
    <?php 
    $img =array("jamie-p",
        "lyod-w",
        "eric-j",
        "shannon-r",
        "peter-k",
        "gretchen-m",
        "nick-v",
        "daanes-development",
        "cheryl-s",
        "richard-and-debra-c",
        "don-m",
        "pat-n",
        "kate-g",
        "tammy-h",
        "allison-m",
        "rhonda-f",
        "ron-t",
        "howard-and-virginia-s",
        "kyle-w",
        "deb-m",
        "jamie-m",
        "kay-b",
        "jane-b",
        "ron-b",
        "larry-c",
        "sue-p",
        "fran-e",
        "richard-l",
        "jacki-b",
        "paula-d",
        "tom-w",
        "sarah-h",
        "steve-h",
        "laurie-b",
        "mindy-s",
        "alphabetsoup-daycare",
        "linda-s",
        "gerald-w",
        "joe-h",
        "sam-and-joann-h",
        "bob-m"	,
        "bob-s",
        "mike-d",
        "phil-m",
        "stephen-r",
        "aaron-t",
        "todd-b",
        "gerald-m",
        "barb-h",
        "robert-p",
        "chuck-m",
        "ed-e",
        "jerry-n",
        "ron-and-marge-d",
        "mike-d-2",
        "sheila-l",
        "mark-c",
        "karen-k",
        "judy-a",
		"pauline-g",
		"kyle-h",
		"patrick-o",
		"joy-h",
		"mark-c-2",
		"joe-h-2",
		"mary-b",
		"william-b",
		"renee-f",
		"susan-s",
		"cindy-m",
		"gerald-m",
		"eloise-a",
		"chuck-w",
		"susan-m",
		"susan-m-2"
		);
        //,	"gloria-h",
        for ($i=sizeof($img)-1; $i>=0; $i--) {?>
         <li class="span4 isotope-element isotope-filter1">
            <div class="thumb-isotope">
                <div class="thumbnail clearfix">
                    <a href="images/testimonials/testimonial-<?php echo $img[$i]; ?>.png">
                        <figure>
                            <img src="images/testimonials/testimonial-<?php echo $img[$i]; ?>.png" alt=""><em></em>
                        </figure>
                        <div class="caption">							
                            <?php
                            $name = explode('-', $img[$i]); 
                            for ($part=0; $part<sizeof($name); $part++) {
                                if ($name[$part] != 'and') {
                                        echo ucfirst($name[$part]);
                                }else{
                                        echo $name[$part];
                                }
                                if ($part+1 < sizeof($name)) {
                                        echo ' ';
                                }else{
                                        if (strlen($name[$part]) == 1) echo '.';
                                }
                            }			
                            ?>
                        </div>
                    </a>				
                </div>
            </div>  
        </li>
    <?php
    } ?>
</ul>


</div>	
</div>	
</div>	
</div>

<?php include('footer.php'); ?>